import org.wltea.analyzer.core.IKSegmenter;
import org.wltea.analyzer.core.Lexeme;

import java.io.IOException;
import java.io.StringReader;

public class Test2 {

	public static String str = "IK Analyzer是一个开源的，基于java语言"
			+ "开发的轻量级的中文分词工具包。从2006年12月推出1.0版开始"
			+ "IKAnalyzer已经推出了4个大版本。最初，它是以开源项目Luence为"
			+ "应用主体的，结合词典分词和文法分析算法的中文分词组件。从3.0版"
			+ "本开始，IK发展为面向Java的公用分词组件，独立于Lucene项目，同时" + "提供了对Lucene的默认优化实现";

	public static void main(String[] args) throws IOException {

		StringReader re = new StringReader(str);
		IKSegmenter ik = new IKSegmenter(re, true);
		Lexeme lex = null;
		while ((lex = ik.next()) != null) {

			System.out.print(lex.getLexemeText() + "|");
		}
	}

}

