import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.wltea.analyzer.lucene.IKAnalyzer;

import java.io.IOException;
import java.io.StringReader;

public class Test {
	public static String str = "IK Analyzer是一个开源的，基于java语言"
			+ "开发的轻量级的中文分词工具包。从2006年12月推出1.0版开始"
			+ "IKAnalyzer已经推出了4个大版本。最初，它是以开源项目Luence为"
			+ "应用主体的，结合词典分词和文法分析算法的中文分词组件。从3.0版"
			+ "本开始，IK发展为面向Java的公用分词组件，独立于Lucene项目，同时"
			+ "提供了对Lucene的默认优化实现";

	public static void main(String[] args) throws IOException {
		// 创建一个分词对象
		Analyzer analyzer = new IKAnalyzer(true);
		StringReader reader = new StringReader(str);
		// 对读入的语言开始进行分词操作
		TokenStream ts = analyzer.tokenStream("", reader);
		// 获得CharTermAttribute类
		CharTermAttribute term = ts.getAttribute(CharTermAttribute.class);
		// 依次遍历分词数据，注意要转换成字符串类型
		while (ts.incrementToken()) {
			System.out.print(term.toString() + "|");
		}
		reader.close();
	}
}
