**指令**

大小有s1,s2,s3,s4，默认s3号大小

所有网站通用
sc                       
sc 4

官网使用
sc 原画 关键词 s4 3



--- 

1. 原画
- dataKey: artwork
- 35页
- 可搜索

2. 漫画作品
- dataKey: comics
- 3页
- 不可搜索

3. 壁纸
- dataKey: wallpapers
- 7页
- 不可搜索

4. 玩家画廊
- dataKey: fanart
- 19页
- 可搜索


---

**原画**
- fruit 水果
- zerg 异虫
- terran 人类
- protoss 星灵
- building 建筑
- cinematic 动画
- environment 场景
- poster 海报
- unit 单位
- planet 行星
- loading-screen 游戏加载画面
- wings-of-liberty 自由之翼
- portrait 头像
- heart-of-the-swarm 虫群之心

玩家画廊
- terran Race: Terran
- zerg Race: Zerg
- kerrigan 角色: 萨拉‧凯瑞甘
- protoss Race: Protoss
- zeratul 角色: 泽拉图
- raynor 角色: 吉姆‧雷诺
