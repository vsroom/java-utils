//package com.example.pojo
//
//import net.mamoe.mirai.console.data.AutoSavePluginData
//import net.mamoe.mirai.console.data.ValueDescription
//import net.mamoe.mirai.console.data.value
//
///**
// * 存储图片分类关键词及匹配词数据
// */
//object TypeSortPluginData : AutoSavePluginData("image-type") {
//    //    原画
//    @ValueDescription("fruit")
//    val artworkFruit: List<String> by value(listOf("水果"))
//
//    @ValueDescription("zerg")
//    val artworkZerg: List<String> by value(listOf("异虫"))
//
//    @ValueDescription("terran")
//    val artworkTerran: List<String> by value(listOf("人类"))
//
//    @ValueDescription("protoss")
//    val artworkProtoss: List<String> by value(listOf("星灵"))
//
//    @ValueDescription("building")
//    val artworkBuilding: List<String> by value(listOf("建筑"))
//
//    @ValueDescription("cinematic")
//    val artworkCinematic: List<String> by value(listOf("动画"))
//
//    @ValueDescription("environment")
//    val artworkEnvironment: List<String> by value(listOf("场景"))
//
//    @ValueDescription("poster")
//    val artworkPoster: List<String> by value(listOf("海报"))
//
//    @ValueDescription("unit")
//    val artworkUnit: List<String> by value(listOf("单位"))
//
//    @ValueDescription("planet")
//    val artworkPlanet: List<String> by value(listOf("行星"))
//
//    @ValueDescription("loading-screen")
//    val artworkLoadingScreen: List<String> by value(listOf("游戏加载画面"))
//
//    @ValueDescription("wings-of-liberty")
//    val artworkWingsOfLiberty: List<String> by value(listOf("自由之翼"))
//
//    @ValueDescription("portrait")
//    val artworkPortrait: List<String> by value(listOf("头像"))
//
//    @ValueDescription("heart-of-the-swarm")
//    val artworkHeartOfTheSwarm: List<String> by value(listOf("虫群之心"))
//
//    // 玩家画廊
//    @ValueDescription("terran")
//    val fanartTerran: List<String> by value(listOf("泰凯斯", "terran"))
//
//    @ValueDescription("zerg")
//    val fanartZerg: List<String> by value(listOf("异虫", "zerg"))
//
//    @ValueDescription("kerrigan")
//    val fanartKerrigan: List<String> by value(listOf("凯瑞甘", "kerrigan"))
//
//    @ValueDescription("protoss")
//    val fanartProtoss: List<String> by value(listOf("星灵", "protoss"))
//
//    @ValueDescription("zeratul")
//    val fanartZeratul: List<String> by value(listOf("泽拉图", "zeratul"))
//
//    @ValueDescription("raynor")
//    val fanartRaynor: List<String> by value(listOf("吉姆", "雷诺", "raynor"))
//}