package pojo;

import com.alibaba.fastjson.annotation.JSONField;

import java.util.Arrays;
import java.util.List;

/**
 * 官网单个图片数据
 */
public final class OWImage {
	private String title;
	private Integer commentOn;
	private String uploadDate;
	private String submit;
	private List<Entity> keywords;
	/** JSON格式的下载链接，需要进一步解析，然后赋给downloadUrl */
	private String detailJson;
	private DownloadUrl downloadUrl;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Integer getCommentOn() {
		return commentOn;
	}

	public void setCommentOn(Integer commentOn) {
		this.commentOn = commentOn;
	}

	public String getUploadDate() {
		return uploadDate;
	}

	public void setUploadDate(String uploadDate) {
		this.uploadDate = uploadDate;
	}

	public String getSubmit() {
		return submit;
	}

	public void setSubmit(String submit) {
		this.submit = submit;
	}

	public List<Entity> getKeywords() {
		return keywords;
	}

	public void setKeywords(List<Entity> keywords) {
		this.keywords = keywords;
	}

	public String getDetailJson() {
		return detailJson;
	}

	public void setDetailJson(String detailJson) {
		this.detailJson = detailJson;
	}

	public DownloadUrl getDownloadUrl() {
		return downloadUrl;
	}

	public void setDownloadUrl(DownloadUrl downloadUrl) {
		this.downloadUrl = downloadUrl;
	}

	@Override
	public String toString() {
		return "OWImage{" +
				"title='" + title + '\'' +
				", commentOn=" + commentOn +
				", uploadDate='" + uploadDate + '\'' +
				", submit='" + submit + '\'' +
				", keywords=" + keywords +
				", detailJson='" + detailJson + '\'' +
				", downloadUrl=" + downloadUrl +
				'}';
	}

	public static final class DownloadUrl {
		@JSONField(name = "index-thumb")
		private String indexThumb;
		private String small;
		private String large;
		private String full;
		private String[] resolutionList;

		public String getIndexThumb() {
			return indexThumb;
		}

		public void setIndexThumb(String indexThumb) {
			this.indexThumb = indexThumb;
		}

		public String getSmall() {
			return small;
		}

		public void setSmall(String small) {
			this.small = small;
		}

		public String getLarge() {
			return large;
		}

		public void setLarge(String large) {
			this.large = large;
		}

		public String getFull() {
			return full;
		}

		public void setFull(String full) {
			this.full = full;
		}

		public String[] getResolutionList() {
			return resolutionList;
		}

		public void setResolutionList(String[] resolutionList) {
			this.resolutionList = resolutionList;
		}

		@Override
		public String toString() {
			return "DownloadUrl{" +
					"indexThumb='" + indexThumb + '\'' +
					", small='" + small + '\'' +
					", large='" + large + '\'' +
					", full='" + full + '\'' +
					", resolutionList=" + Arrays.toString(resolutionList) +
					'}';
		}
	}
}
