package pojo;

/**
 * 键值对，主要用于接收JSON数据
 */
public class Entity {
	private final String key;
	private final String value;

	public Entity(String key, String value) {
		this.key = key;
		this.value = value;
	}

	public String getKey() {
		return key;
	}

	public String getValue() {
		return value;
	}

	@Override
	public String toString() {
		return "Entity{" +
				"key='" + key + '\'' +
				", value='" + value + '\'' +
				'}';
	}
}
