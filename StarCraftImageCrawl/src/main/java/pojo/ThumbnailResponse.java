package pojo;

import java.util.List;

/**
 * 缩略图响应数据
 */
public class ThumbnailResponse {
	private String dataKey;
	private List<ImageSource> medias;

	public String getDataKey() {
		return dataKey;
	}

	public void setDataKey(String dataKey) {
		this.dataKey = dataKey;
	}

	public List<ImageSource> getMedias() {
		return medias;
	}

	public void setMedias(List<ImageSource> medias) {
		this.medias = medias;
	}

	@Override
	public String toString() {
		return "ThumbnailResponse{" +
				"dataKey='" + dataKey + '\'' +
				", medias=" + medias +
				'}';
	}
}

