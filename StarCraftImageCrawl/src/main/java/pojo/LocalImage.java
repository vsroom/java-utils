package pojo;

/**
 * 下载后的本地图片
 */
public class LocalImage {
	/** 文件全名 */
	private String name;
	/** 对应图片的下载源 */
	private ImageSource imageSource;

	public LocalImage() {
	}

	public LocalImage(String name, ImageSource imageSource) {
		this.name = name;
		this.imageSource = imageSource;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ImageSource getImageSource() {
		return imageSource;
	}

	public void setImageSource(ImageSource imageSource) {
		this.imageSource = imageSource;
	}

	@Override
	public String toString() {
		return "LocalImage{" +
				"name='" + name + '\'' +
				", imageSource=" + imageSource +
				'}';
	}
}
