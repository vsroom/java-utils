package pojo;

/**
 * 用户输入的指令
 */
// @Data
// @NoArgsConstructor
// @AllArgsConstructor
public class CommandData {
	/** 指令 */
	public static final String COMMAND = "sc";
	/** 所属分类，如果存在的话 */
	private String sort;
	/** 搜索关键词，如果支持的话 */
	private String keyword;
	/** 图片大小，默认为第2大的图片，如果大小可选的话 */
	private SIZE size = SIZE.S3;
	/** 图片数量，默认取1张 */
	private int count = 1;

	public CommandData() {
	}

	public String getSort() {
		return sort;
	}

	public void setSort(String sort) {
		this.sort = sort;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public SIZE getSize() {
		return size;
	}

	public void setSize(SIZE size) {
		this.size = size;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	/** 图片大小，越来越大 */
	public enum SIZE {
		S1, S2, S3, S4;

		@Override
		public String toString() {
			return name().toLowerCase();
		}
	}

	@Override
	public String toString() {
		return "CommandData{" +
				"sort='" + sort + '\'' +
				", keyword='" + keyword + '\'' +
				", size=" + size +
				", count=" + count +
				'}';
	}
}
