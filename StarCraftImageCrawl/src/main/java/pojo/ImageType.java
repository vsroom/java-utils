package pojo;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum ImageType {
	/** 原画 */
	ARTWORK("原画", 35, null),
	/** 漫画作品 */
	COMICS("漫画作品", 3, null),
	/** 壁纸 */
	WALLPAPERS("壁纸", 7, null),
	/** 玩家画廊 */
	FANART("玩家画廊", 19, null);

	static {
		// 搜索关键词读取配置文件
		HashMap<String, List<String>> artworkSort = new HashMap<>();
		artworkSort.put("fruit", List.of("水果"));
		artworkSort.put("zerg", List.of("异虫"));
		artworkSort.put("terran", List.of("人类"));
		artworkSort.put("protoss", List.of("星灵"));
		artworkSort.put("building", List.of("建筑"));
		artworkSort.put("cinematic", List.of("动画"));
		artworkSort.put("environment", List.of("场景"));
		artworkSort.put("poster", List.of("海报"));
		artworkSort.put("unit", List.of("单位"));
		artworkSort.put("planet", List.of("行星"));
		artworkSort.put("loading-screen", List.of("游戏加载画面"));
		artworkSort.put("wings-of-liberty", List.of("自由之翼"));
		artworkSort.put("portrait", List.of("头像"));
		artworkSort.put("heart-of-the-swarm", List.of("虫群之心"));
		ARTWORK.typeSort = artworkSort;

		HashMap<String, List<String>> fanArtSort = new HashMap<>();
		fanArtSort.put("terran", List.of("泰凯斯", "terran"));
		fanArtSort.put("zerg", List.of("异虫", "zerg"));
		fanArtSort.put("kerrigan", List.of("凯瑞甘", "kerrigan"));
		fanArtSort.put("protoss", List.of("星灵", "protoss"));
		fanArtSort.put("zeratul", List.of("泽拉图", "zeratul"));
		fanArtSort.put("raynor", List.of("吉姆", "雷诺", "raynor"));
		FANART.typeSort = fanArtSort;
	}

	private final String description;
	/**
	 * 对应图片的总页数，
	 * 这里写死是认为官网的图片不会再增加了，不会变了
	 */
	private final int pageCount;
	/** 匹配分类关键词的词语，键为关键词，值为用户能匹配该关键词的词，不支持搜索就填null */
	private Map<String, List<String>> typeSort;

	ImageType(String description, int pageCount, Map<String, List<String>> typeSort) {
		this.description = description;
		this.pageCount = pageCount;
		this.typeSort = typeSort;
	}

	public String getDescription() {
		return description;
	}

	public int getPageCount() {
		return pageCount;
	}

	public Map<String, List<String>> getTypeSort() {
		return typeSort;
	}

	/** 转为枚举变量的小写版本， 可以直接用于作为dataKey的参数 */
	@Override
	public String toString() {
		return name().toLowerCase();
	}
}
