package pojo;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * 图片实体类，对应网络资源
 */
public class ImageSource {
	/** 图片所属ID */
	private String id;
	/** 图片标题 */
	private String title;
	/** 不同类型图片或不同大小图片下载链接 */
	@JSONField(name = "index-thumb")
	private String downloadLink;

	public ImageSource() {
	}

	public ImageSource(String id, String title, String downloadLink) {
		this.id = id;
		this.title = title;
		this.downloadLink = downloadLink;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDownloadLink() {
		return downloadLink;
	}

	public void setDownloadLink(String downloadLink) {
		this.downloadLink = downloadLink;
	}

	@Override
	public String toString() {
		return "ImageSource{" +
				"id='" + id + '\'' +
				", title='" + title + '\'' +
				", downloadLink=" + downloadLink +
				'}';
	}
}
