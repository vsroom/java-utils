package service;

import pojo.CommandData;
import pojo.ImageSource;
import pojo.LocalImage;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;

/**
 * 对用户放输入提供服务
 */
public interface ImageService {
	ThreadLocalRandom random = ThreadLocalRandom.current();
	/** 保存路径 */
	String SAVE_PATH = "./data/com.example.star-craft-image-crawl/cache";
	/** 图片常用后缀，用于判断本地是否已有要下载图片，只要文件名符合即可，后缀不限 */
	List<String> COMMON_SUFFIX = Arrays.asList("jpg", "png", "gif", "webp");

	/**
	 * 下载图片的第一步，根据用户指令爬取图片数据
	 *
	 * @param command 用户输入解析后的指令对象
	 * @return 搜索到的图片资源
	 */
	List<ImageSource> getImageDownloadLink(CommandData command) throws IOException;

	/**
	 * 下载图片的第二步，保存图片到本地
	 *
	 * @param command      用户指令对象
	 * @param imageSources 图片资源信息
	 * @return 下载后的图片数据
	 */
	List<LocalImage> downloadImage(CommandData command, List<ImageSource> imageSources);

	/**
	 * 获取分类列表，支持分类的网站提供这个分类关键词。键为用户输入，值为网络请求需要的分类关键字
	 */
	Map<String, String> getSortMap();
}
