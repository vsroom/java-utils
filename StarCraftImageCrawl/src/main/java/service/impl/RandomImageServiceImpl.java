package service.impl;

import org.jetbrains.annotations.Nullable;
import pojo.CommandData;
import pojo.ImageSource;
import pojo.ImageType;
import pojo.LocalImage;
import service.ImageService;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;

/**
 * 随机壁纸。
 * <p>
 * 原本是打算是随机一个网站随机一个壁纸的，但是想了之后觉得有些网站访问速度较慢，并且访问数据量差距较大，因此这里只访问官网的，不仅快，还高效
 */
public class RandomImageServiceImpl implements ImageService {
	public static final RandomImageServiceImpl INSTANCE = new RandomImageServiceImpl();
	/** 从中随机选一个服务 */
	// public static final List<ImageService> imageServices = new ArrayList<>();
	// static {
	// 	imageServices.add(OfficialWebsiteImageServiceImpl.INSTANCE);
	// 	imageServices.add(WallpaperAbyssImageServiceImpl.INSTANCE);
	// }
	public static final ImageService imageService = OfficialWebsiteImageServiceImpl.INSTANCE;
	private static final ThreadLocalRandom random = ThreadLocalRandom.current();

	private RandomImageServiceImpl() {
	}

	@Override
	public List<ImageSource> getImageDownloadLink(@Nullable CommandData command) throws IOException {
		ImageType[] sort = ImageType.values();
		command = new CommandData();
		command.setSort(sort[random.nextInt(sort.length)].toString());
		return imageService.getImageDownloadLink(command);
	}

	@Override
	public List<LocalImage> downloadImage(CommandData command, List<ImageSource> imageSources) {
		return imageService.downloadImage(command, imageSources);
	}

	@Override
	public Map<String, String> getSortMap() {
		return imageService.getSortMap();
	}
}
