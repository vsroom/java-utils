package command;

import pojo.CommandData;
import pojo.ImageSource;
import pojo.ImageType;
import pojo.LocalImage;
import service.ImageService;
import service.impl.OfficialWebsiteImageServiceImpl;
import service.impl.RandomImageServiceImpl;
import service.impl.WallpaperAbyssImageServiceImpl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ImageCrawlCommand {
	public static final ImageCrawlCommand INSTANCE = new ImageCrawlCommand();
	public static final String COMMAND = "sc";
	/** WallpaperAbyss网 */
	public static final ImageService waImageService = WallpaperAbyssImageServiceImpl.INSTANCE;
	/** 随机 */
	public static final ImageService rImageService = RandomImageServiceImpl.INSTANCE;
	/** 官网 */
	private static final ImageService owImageService = OfficialWebsiteImageServiceImpl.INSTANCE;

	private ImageCrawlCommand() {
	}

	/**
	 * 解析指令，将空格为间隔符字符串解析为CommandData对象
	 */
	private static CommandData parseCommand(List<String> command) {
		CommandData data = new CommandData();
		Map<String, String> map = owImageService.getSortMap();
		// p作为一个指针，在参数之间移动
		int index = 0;
		String p = command.get(index++);

		if (map.containsKey(p)) {
			// 官网图片，格式为：分类(*) 关键词 大小 数量
			// 存在分类
			data.setSort(map.get(p));
			if (command.size() <= index) return data;
			p = command.get(index++);

			try {
				ImageType imageType = ImageType.valueOf(data.getSort().toUpperCase());
				Map<String, List<String>> typeSort = imageType.getTypeSort();
				for (Map.Entry<String, List<String>> entry : typeSort.entrySet()) {
					if (entry.getValue().contains(p)) {
						// 存在关键词
						data.setKeyword(entry.getKey());

						if (command.size() <= index) return data;
						p = command.get(index++);
						break;
					}
				}
			} catch (IllegalArgumentException e) {
				// 解析失败，第二个参数不是关键词
				e.printStackTrace();
			}

			CommandData.SIZE[] sizes = CommandData.SIZE.values();
			for (CommandData.SIZE size : sizes) {
				if (p.equals(size.toString())) {
					// 存在大小
					data.setSize(size);
					if (command.size() <= index) return data;
					p = command.get(index++);
				}
			}

		}
		// 其他网站图片，格式为：数字，这里最多取10张
		try {
			int count = Integer.parseInt(p);
			data.setCount(Math.min(count, 10));
		} catch (NumberFormatException e) {
			// 参数不对
			e.printStackTrace();
		}
		return data;
	}

	private static List<LocalImage> commandProcess(CommandData command, ImageService imageService) throws RuntimeException {
		List<ImageSource> imageSource;
		try {
			imageSource = imageService.getImageDownloadLink(command);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		return imageService.downloadImage(command, imageSource);
	}

	public static void main(String[] args) {
		ImageCrawlCommand.INSTANCE.onCommand(List.of());
	}

	public void onCommand(List<String> command) {
		List<LocalImage> res = null;
		// 随机一张图片，命令对象数据由rImageService填充
		if (command.size() == 0) {
			res = commandProcess(new CommandData(), rImageService);
		} else {
			try {
				CommandData commandData = parseCommand(command);
				System.out.println(commandData);
				if (commandData.getSort() == null) //其他网站，现在只有WallpaperAbyss
					res = commandProcess(commandData, waImageService);
				else //官网
					res = commandProcess(commandData, owImageService);
			} catch (RuntimeException e) {
				System.err.println(e.getMessage());
			}
		}
		for (LocalImage localImage : res) {
			System.out.println(localImage);
		}
	}
}
