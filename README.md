# Java常用小工具集合

**清单**
- AnnotationTranslate 单词注释翻译
- FileExtract 保留目录结构下将文件夹中指定格式的文件拷贝到另一个文件夹下
- GameCrawl 盗版游戏爬取
- JSONOper JSON字符串比较
- LarkMarkdownUpload 飞书批量上传markdown应用
- PackageRename 项目包名批量重命名
- QRCode 二维码的生成和解码
- segment 分句工具
- StarCraftImageCrawl 星际争霸2图片爬取
- VoiceSynthesis 百度语音合成