/**
 * 游戏信息类
 */
public class GameDetail {
	// 游戏对应ID
	private String id;
	private String title;
	// 凭证，购买后才会给，是一个时间戳，例如20220828173501
	private String genkey;
	// 下载链接
	private String url;

	public GameDetail() {
	}

	public GameDetail(String id, String title) {
		this.id = id;
		this.title = title;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getGenkey() {
		return genkey;
	}

	public void setGenkey(String genkey) {
		this.genkey = genkey;
	}

	@Override
	public String toString() {
		return "GameDetail{" +
				"id='" + id + '\'' +
				", title='" + title + '\'' +
				", genkey='" + genkey + '\'' +
				", url='" + url + '\'' +
				'}';
	}
}
