package entity;

import com.alibaba.fastjson.JSONObject;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TransResponse {
	private Data data;
	private String errorMessage;
	private Integer errorCode;

	@lombok.Data
	@NoArgsConstructor
	@AllArgsConstructor
	public static class Data {
		private String translation;
	}

	public static TransResponse parse(String jsonData) {
		TransResponse transResponse = JSONObject.parseObject(jsonData, TransResponse.class);
		JSONObject jsonObject = JSONObject.parseObject(jsonData);
		transResponse.setData(jsonObject.getObject("data", Data.class));
		return transResponse;
	}
}
