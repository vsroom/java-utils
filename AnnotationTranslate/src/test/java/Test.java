import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Test {
	// Doc注释开始符
	public static final Pattern DOC_START = Pattern.compile("^[\\t ]*/\\*+.*");
	// Doc注释结束符
	public static final Pattern DOC_END = Pattern.compile(" *\\*/\\s*$");
	// 匹配位于行首的注释中的Doc注释，去掉前面的 *
	public static final Pattern TOP_OF_LINE = Pattern.compile("^\\s*(@\\S+\\s+)(.+)");
	// Doc注释
	public static final Pattern LINE1 = Pattern.compile("^[\\t ]*\\*");
	// 单行注释
	public static final Pattern LINE2 = Pattern.compile("^[\\t ]*//");

	// 文本中的Doc注释
	public static final Pattern DOC_IN_ANNO = Pattern.compile("\\{@\\S+\\s+.+?\\}");
	// 文本中的方法
	public static final Pattern METHOD_IN_ANNO = Pattern.compile(" (\\S+\\(\\)) ");

	/** 表示一条注释相关信息 */
	static LinkedList<Block> list = new LinkedList<>();

	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new FileReader("D:\\MyDefault\\desktop\\jd\\StyledListCellRenderer.java"));
		String line;

		// 解析注释
		while ((line = br.readLine()) != null) {
			int start = -1, end = -1;
			Matcher ms = DOC_START.matcher(line);
			Matcher me = DOC_END.matcher(line);
			if (ms.find()) start = ms.end();
			if (me.find()) end = me.start();

			if (start != -1 && end != -1) {
				list.add(new Block(null, Style.DOC_START));
				// 只有一行的注释
				String content = line.substring(start, Math.max(start, end));
				if (!content.equals("")) { // 空注释不处理
					list.add(new Block("", Style.DOC));
					list.add(new Block(content, Style.PENDING));
				}
				list.add(new Block(null, Style.DOC_END));
				// 处理该条注释
				processBlocks();
			} else if (start != -1) {
				list.add(new Block(null, Style.DOC_START));
				// 注释的第一行
				if (!line.substring(start).trim().equals("")) {
					list.add(new Block(line.substring(start), Style.PENDING));
				}
			} else if (end != -1) {
				// 注释的最后一行
				String content = line.substring(0, end);
				if (!content.trim().equals("")) {
					Block last = list.getLast();

					// 含有@注释的不追加
					if (last.style == Style.PENDING) {
						last.setText(last.getText() + " " + content);
					} else {
						list.add(new Block(content, Style.PENDING));
					}
				}
				list.add(new Block(null, Style.DOC_END));

				// 处理该条注释
				processBlocks();
			} else {
				Matcher matcher1 = LINE1.matcher(line);
				Matcher matcher2 = LINE2.matcher(line);

				if (matcher1.find()) {
					// Doc注释 *
					String content = line.substring(matcher1.end());
					Matcher m = TOP_OF_LINE.matcher(content);
					if (m.find()) {
						// 含有@ 标签
						list.add(new Block(m.group(1), Style.DOC));
						list.add(new Block(m.group(2), Style.PENDING));
					} else {
						// 不含@标签，连接到最后一条字符上
						Block last = list.getLast();
						if (last.style == Style.PENDING && !content.trim().equals("")) {
							last.setText(last.getText() + " " + content);
						} else {
							if (last.style == Style.PENDING)
								last.setText(last.getText() + "<br/>"); //空行Doc注释在上一条字符串后加换行标签
							list.add(new Block("", Style.DOC));
							list.add(new Block(content, Style.PENDING));
						}
					}
				} else if (matcher2.find()) {
					// 行内注释 //
					list.add(new Block(null, Style.LINE));
					list.add(new Block(line.substring(matcher2.end()), Style.PENDING));

					// 处理该条注释
					processBlocks();
				} else {
					// 不是注释
				}
			}
		}

		br.close();
	}

	private static LinkedList<Block> parseText(LinkedList<Block> oldList) {
		LinkedList<Block> newList = new LinkedList<>();

		for (Block block : oldList) {
			if (block.style != Style.PENDING) {
				newList.add(block);
			} else {
				// 处理文本
				processDocInAnno(newList, block.text);
				newList.add(new Block("", Style.WRAP));
			}
		}

		return newList;
	}

	/** 处理给定文本中的行内Doc标签 */
	private static void processDocInAnno(LinkedList<Block> list, String s) {
		Matcher matcher = DOC_IN_ANNO.matcher(s);
		if (matcher.find()) {
			// 含有行内doc
			processMethod(list, s.substring(0, matcher.start()));
			list.add(new Block(matcher.group(), Style.DOC_IN_LINE));
			int e = matcher.end();
			while (matcher.find()) {
				processMethod(list, s.substring(e, matcher.start()));
				list.add(new Block(matcher.group(), Style.DOC_IN_LINE));
				e = matcher.end();
			}
			processMethod(list, s.substring(e));
		} else {
			processMethod(list, s);
		}
	}

	/** 处理文本中未使用标签包含的方法，需特殊处理防止被翻译 */
	private static void processMethod(LinkedList<Block> list, String s) {
		Matcher matcher = METHOD_IN_ANNO.matcher(s);
		if (matcher.find()) {
			// 含有行内doc
			list.add(new Block(s.substring(0, matcher.start()), Style.TEXT));
			list.add(new Block(matcher.group(), Style.METHOD));
			int e = matcher.end();
			while (matcher.find()) {
				list.add(new Block(s.substring(e, matcher.start()), Style.TEXT));
				list.add(new Block(matcher.group(), Style.METHOD));
				e = matcher.end();
			}
			list.add(new Block(s.substring(e), Style.TEXT));
		} else {
			list.add(new Block(s, Style.TEXT));
		}
	}

	private static void processBlocks() {
		list = parseText(list);

		// 翻译注释
		StringBuilder builder = new StringBuilder();
		for (Block block : list) {
			switch (block.style) {
				case DOC_START -> {
					builder.append("/**\n");
				}
				case DOC_END -> {
					builder.append("*/\n");
				}
				case DOC -> {
					builder.append("* ").append(block.text);
				}
				case LINE -> {
					builder.append("// ");
				}
				case WRAP -> {
					builder.append("\n");
				}
				default -> {
					builder.append(block.text);
				}
			}
		}

		System.out.println(builder);
		list.clear();
	}

	@Data
	@NoArgsConstructor
	@AllArgsConstructor
	public static class Block {
		String text;
		Style style;

		// @Override
		// public String toString() {
		// 	return text;
		// }

		@Override
		public String toString() {
			return "Block{" +
					"text='" + text + '\'' +
					", style=" + style +
					'}';
		}
	}

	public enum Style {
		/** Doc注释开始符 */
		DOC_START,
		/** Doc注释结束符 */
		DOC_END,
		/** 单行注释 */
		LINE,
		/** 待处理的 */
		PENDING,
		/** 含有Doc的*文本 */
		DOC,

		/** 文本 */
		TEXT,
		/** 换行 */
		WRAP,
		/** 行内标签 */
		DOC_IN_LINE,
		/** 没有标签的方法 */
		METHOD,
	}
}
