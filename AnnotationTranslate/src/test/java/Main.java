import static util.TranslateUtil.translate;

public class Main {
	public static void main(String[] args) {
		String line = """
				  Creates a BannerPanel with title, subtitle and component.
				  <separator />
				  @param title         
				  <separator />
				  the title.
				  @param subtitle      
				  <separator />
				  the sub title.
				  @param 
				  <separator />
				  iconComponent the icon component. It will appear where the icon is if using constructor {@link
				                       #BannerPanel(String,String,javax.swing.ImageIcon)}.
				""";

		System.out.println(translate(line));
	}
}
