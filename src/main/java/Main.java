import javax.swing.*;
import java.awt.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main extends JFrame {
	public Main() throws HeadlessException {
		String document = """
				<html>
				 <head>
				  <script>alert('管理员设定了VIP购买间隔，请57秒后再试！');history.back();</script>
				 </head>
				 <body></body>
				</html>""";

		Pattern pattern = Pattern.compile("alert\\('(.+)'\\);");
		Matcher matcher = pattern.matcher(document);
		if (matcher.find())
			System.out.println(matcher.group(1));
		else
			System.out.println("未找到");
	}

	public static void main(String[] args) {
		EventQueue.invokeLater(() -> {
			new Main().setVisible(true);
		});
	}
}
