package code;

import com.formdev.flatlaf.FlatLightLaf;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.io.File;

/**
 * 二维码生成和解析小程序
 */
public class QrCodeTest extends JFrame {
	JButton geCode = new JButton("生成二维码");
	JButton deCode = new JButton("解析二维码");
	JTextArea textArea = new JTextArea(5, 40);
	JLabel textLabel = new JLabel("-------------------- 二维码内容 --------------------");
	JLabel imgLabel = new JLabel("图片路径");
	JTextField imgField = new JTextField(30);
	JButton imgButton = new JButton("选择图片");
	JLabel destLabel;
	JTextField destField;
	JButton destButton;
	JPanel panel;
	JButton returnButton = new JButton("返回");

	JFileChooser fileChooser = new JFileChooser();

	public QrCodeTest() throws HeadlessException {
		setBounds(300, 300, 500, 350);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setTitle("二维码生成/解析器");
		panel = new JPanel(new FlowLayout());
		add(panel);

		geCode.setPreferredSize(new Dimension(250, 60));
		deCode.setPreferredSize(new Dimension(250, 60));
		Font font = new Font(Font.DIALOG, Font.BOLD, 20);
		geCode.setFont(font);
		deCode.setFont(font);

		panel.add(geCode);
		panel.add(deCode);

		// 共有的初始化操作
		textLabel.setPreferredSize(new Dimension(400, 20));
		textLabel.setHorizontalAlignment(SwingConstants.CENTER);
		textArea.setLineWrap(true);

		fileChooser.setCurrentDirectory(new File("."));
		geCode.addActionListener((event) -> generateCode());
		deCode.addActionListener((e -> analystCode()));
	}

	// 生成二维码
	public void generateCode() {
		geCode.setVisible(false); //这里隐藏掉只是我单纯地不会删除组件
		deCode.setVisible(false);

		destLabel = new JLabel("保存路径");
		destField = new JTextField(30);
		destButton = new JButton("选择路径");
		geCode = new JButton("生成二维码");

		panel.add(textLabel);
		panel.add(textArea);
		panel.add(imgLabel);
		panel.add(imgField);
		panel.add(imgButton);
		panel.add(destLabel);
		panel.add(destField);
		panel.add(destButton);
		panel.add(geCode);
		add(returnButton, BorderLayout.SOUTH);

		addImgButtonListener(imgButton);
		// 保存目录
		destButton.addActionListener(e -> {
			int ret = fileChooser.showSaveDialog(this);
			if (ret == JFileChooser.APPROVE_OPTION) {
				File file = fileChooser.getSelectedFile();
				destField.setText(file.getAbsolutePath());
			}
		});
		// 生成二维码
		geCode.addActionListener(e -> {
			String imgPath = imgField.getText();
			String destPath = destField.getText();
			File destFile = new File(destPath);
			if (!"".equals(imgPath) && (!new File(imgPath).isFile() || !(imgPath.endsWith(".png") || imgPath.endsWith(".jpg")))) {
				JOptionPane.showMessageDialog(this, "图片路径存在问题", "错误", JOptionPane.ERROR_MESSAGE);
				return;
			}

			if (destFile.isFile() || destFile.isDirectory()) {
				JOptionPane.showMessageDialog(this, "填入的路径已经存在该文件或为一个目录", "错误", JOptionPane.ERROR_MESSAGE);
				return;
			}
			if ("".equals(destPath) || !destFile.getParentFile().isDirectory()) {
				JOptionPane.showMessageDialog(this, "保存路径不存在", "错误", JOptionPane.ERROR_MESSAGE);
				return;
			}
			if (!destPath.endsWith(".jpg"))
				destPath += ".jpg";
			//生成二维码
			try {
				QRCodeUtil.encode(textArea.getText(), imgPath, destPath, true);
				JOptionPane.showConfirmDialog(this, "二维码生成成功");
			} catch (Exception ex) {
				JOptionPane.showMessageDialog(this, ex.getMessage(), "错误", JOptionPane.ERROR_MESSAGE);
			}
		});
		returnButton.addActionListener(e -> {
			new QrCodeTest().setVisible(true);
			this.dispose();
		});
	}

	// 解析二维码
	public void analystCode() {
		geCode.setVisible(false);
		deCode.setVisible(false);

		deCode = new JButton("解析二维码");

		textArea.setLineWrap(true);
		panel.add(imgLabel);
		panel.add(imgField);
		panel.add(imgButton);
		panel.add(deCode);
		panel.add(textLabel);
		panel.add(textArea);
		add(returnButton, BorderLayout.SOUTH);

		addImgButtonListener(imgButton);
		deCode.addActionListener(e -> {
			String imgPath = imgField.getText();
			if (!new File(imgPath).isFile() || !(imgPath.endsWith(".png") || imgPath.endsWith(".jpg"))) {
				JOptionPane.showMessageDialog(this, "图片路径存在问题", "错误", JOptionPane.ERROR_MESSAGE);
				return;
			}
			try {
				textArea.setText(QRCodeUtil.decode(imgPath));
			} catch (Exception ex) {
				JOptionPane.showMessageDialog(this, "图片未扫描到二维码", "错误", JOptionPane.ERROR_MESSAGE);
			}
		});
		returnButton.addActionListener(e -> {
			new QrCodeTest().setVisible(true);
			this.dispose();
		});
	}

	// 为查找图片添加监听器，因为两个方法都需要写，故提取出来
	private void addImgButtonListener(JButton imgButton) {
		imgButton.addActionListener(e -> {
			FileNameExtensionFilter filter = new FileNameExtensionFilter("图片文件", "jpg", "png");
			fileChooser.setFileFilter(filter);

			int ret = fileChooser.showOpenDialog(this);
			if (ret == JFileChooser.APPROVE_OPTION) {
				File file = fileChooser.getSelectedFile();
				imgField.setText(file.getAbsolutePath());
			}
		});
	}

	public static void main(String[] args) throws Exception {
		FlatLightLaf.setup();
		// // 存放在二维码中的内容
		// String text = "我是小铭";
		// // 嵌入二维码的图片路径
		// String imgPath = "G:/qrCode/dog.jpg";
		// // 生成的二维码的路径及名称
		// String destPath = "C:\\Users\\Administrator\\Desktop\\1.jpg";
		// //生成二维码
		// QRCodeUtil.encode(text, imgPath, destPath, true);
		// // 解析二维码
		// String str = QRCodeUtil.decode(destPath);
		// // 打印出解析出的内容
		// System.out.println(str);
		new QrCodeTest().setVisible(true);
	}
}
