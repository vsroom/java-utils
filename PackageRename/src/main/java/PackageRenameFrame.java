import com.formdev.flatlaf.FlatLightLaf;
import net.miginfocom.swing.MigLayout;
import org.jdesktop.swingx.JXTextField;

import javax.swing.*;
import java.awt.*;
import java.io.File;

/**
 * 为项目所有包重命名
 * <p>
 * 需要注意
 * <ul>
 *     <li>目录路径使用反斜杠</li>
 *     <li>只有在 src\main\java 下的java文件会被重命名</li>
 * </ul>
 */
public class PackageRenameFrame extends JFrame {
	JXTextField pathField = new JXTextField("目录路径");
	JButton chooseButton = new JButton("选择");
	JFileChooser chooser = new JFileChooser();
	JButton button = new JButton("转换");
	JLabel progress = new JLabel("0%");

	public PackageRenameFrame() {
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setTitle("类包名重命名");

		JPanel p = new JPanel(new MigLayout("wrap 1", "grow, center", "[]15[]30[]30[]"));

		p.add(pathField, "split 2, w 400::");
		p.add(chooseButton);
		p.add(button);
		p.add(progress, "left");

		setListener();
		getContentPane().add(p);
		pack();
		setLocationRelativeTo(null);
	}

	private void setListener() {
		chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

		chooseButton.addActionListener(e -> {
			if (chooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
				File f = chooser.getSelectedFile();
				pathField.setText(f.getAbsolutePath());
			}
		});

		button.addActionListener(e -> {
			new RenameWorker(new File(pathField.getText()), button, progress).execute();
			button.setEnabled(false);
		});
	}

	public static void main(String[] args) {
		EventQueue.invokeLater(() -> {
			FlatLightLaf.setup();
			new PackageRenameFrame().setVisible(true);
		});
	}
}
