package service;

import entity.data.DocFile;
import entity.inte.ResponseData;
import entity.inte.mycloud.file.DeleteFileResponse;
import entity.inte.mycloud.imports.CreateImportTasksRequest;
import entity.inte.mycloud.imports.CreateImportTasksResponse;
import entity.inte.mycloud.imports.QueryImportTasksResponse;
import entity.inte.mycloud.upload.UploadFileRequest;
import entity.inte.mycloud.upload.UploadFileResponse;

public interface FileService {
	/**
	 * 上传文件
	 */
	UploadFileResponse uploadFile(UploadFileRequest uploadFileRequest, int count);

	/**
	 * 创建导入任务
	 */
	CreateImportTasksResponse createImportTasks(CreateImportTasksRequest createImportTasksRequest, int count);

	/**
	 * 查询导入任务
	 */
	QueryImportTasksResponse queryImportTasks(CreateImportTasksResponse createImportTasksResponse, int count);

	/**
	 * 删除上传文件
	 */
	DeleteFileResponse deleteDoc(UploadFileResponse uploadFileResponse, int count);

	/**
	 * 批量更新块（文本链接和图片）
	 */
	ResponseData<String> batchUpdateProcess(DocFile docFile, int count);

	/**
	 * 上传图片素材
	 */
	UploadFileResponse uploadImageMedia(DocFile docFile, int count);
}
