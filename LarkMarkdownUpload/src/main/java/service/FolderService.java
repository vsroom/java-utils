package service;

import entity.inte.mycloud.folder.CreateFolderRequest;
import entity.inte.mycloud.folder.CreateFolderResponse;

public interface FolderService {
	CreateFolderResponse createFolder(CreateFolderRequest createFolderRequest);
}
