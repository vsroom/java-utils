package util;

import org.apache.log4j.HTMLLayout;
import org.apache.log4j.Layout;

import java.util.Date;

public class MyHTMLLayout extends HTMLLayout {
	@Override
	public String getHeader() {
		StringBuilder sbuf = new StringBuilder();
		sbuf.append("<!DOCTYPE html>").append(Layout.LINE_SEP)
				.append("<html lang=\"en\">").append(Layout.LINE_SEP)
				.append("<head>").append(Layout.LINE_SEP)
				.append("<title>").append(getTitle()).append("</title>").append(Layout.LINE_SEP)
				.append("<meta charset=\"UTF-8\">").append(Layout.LINE_SEP)
				.append("<style type=\"text/css\">").append(Layout.LINE_SEP)
				.append("<!--").append(Layout.LINE_SEP)
				.append("body, table {font-family: arial,sans-serif; font-size: x-small;}").append(Layout.LINE_SEP)
				.append("th {background: #336699; color: #FFFFFF; text-align: left;}").append(Layout.LINE_SEP)
				.append("-->").append(Layout.LINE_SEP)
				.append("</style>").append(Layout.LINE_SEP)
				.append("</head>").append(Layout.LINE_SEP)
				.append("<body bgcolor=\"#FFFFFF\" topmargin=\"6\" leftmargin=\"6\">").append(Layout.LINE_SEP)
				.append("<hr size=\"1\" noshade>").append(Layout.LINE_SEP)
				.append("Log session start time ").append(new Date()).append("<br>").append(Layout.LINE_SEP)
				.append("<br>").append(Layout.LINE_SEP)
				.append("<table cellspacing=\"0\" cellpadding=\"4\" border=\"1\" bordercolor=\"#224466\" width=\"100%\">").append(Layout.LINE_SEP)
				.append("<tr>").append(Layout.LINE_SEP)
				.append("<th>Time</th>").append(Layout.LINE_SEP)
				.append("<th>Thread</th>").append(Layout.LINE_SEP)
				.append("<th>Level</th>").append(Layout.LINE_SEP)
				.append("<th>Category</th>").append(Layout.LINE_SEP);
		if (getLocationInfo()) {
			sbuf.append("<th>File:Line</th>").append(Layout.LINE_SEP);
		}

		sbuf.append("<th>Message</th>").append(Layout.LINE_SEP).append("</tr>").append(Layout.LINE_SEP);
		return sbuf.toString();
	}
}
