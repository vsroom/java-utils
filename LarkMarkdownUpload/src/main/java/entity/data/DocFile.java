package entity.data;

import entity.inte.mycloud.file.DeleteFileResponse;
import entity.inte.mycloud.imports.CreateImportTasksRequest;
import entity.inte.mycloud.imports.CreateImportTasksResponse;
import entity.inte.mycloud.imports.QueryImportTasksResponse;
import entity.inte.mycloud.upload.UploadFileRequest;
import entity.inte.mycloud.upload.UploadFileResponse;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Map;

/**
 * 文件，从本地文件到云文档的所有需要的数据
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DocFile {
	// 这里不能使用@Data，相互引用，toString时会堆栈溢出
	private DocFolder parent;
	/**
	 * 本地图片，为文件中URL链接到本地图片文件的映射
	 * key:原链接拼接后的伪造的https链接，只是为了让飞书认为是一个链接
	 * value:key对应的本地资源文件的绝对路径，获取文件块的JSON数据后可以直接用该映射替换伪造链接
	 */
	private Map<String, String> resourceMap;
	private UploadFileRequest uploadFileRequest;
	private UploadFileResponse uploadFileResponse;
	private CreateImportTasksRequest createImportTasksRequest;
	private CreateImportTasksResponse createImportTasksResponse;
	private QueryImportTasksResponse queryImportTasksResponse;
	private DeleteFileResponse deleteFileResponse;

	@Override
	public String toString() {
		return "DocFile{" +
				"parent=" + (parent == null?null:parent.getDir()) +
				", resourceMap=" + resourceMap +
				", uploadFileRequest=" + uploadFileRequest +
				", uploadFileResponse=" + uploadFileResponse +
				", createImportTasksRequest=" + createImportTasksRequest +
				", createImportTasksResponse=" + createImportTasksResponse +
				", queryImportTasksResponse=" + queryImportTasksResponse +
				", deleteFileResponse=" + deleteFileResponse +
				'}';
	}
}
