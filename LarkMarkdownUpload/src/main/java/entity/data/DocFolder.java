package entity.data;

import entity.inte.mycloud.folder.CreateFolderRequest;
import entity.inte.mycloud.folder.CreateFolderResponse;
import lombok.*;

import java.io.File;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ConcurrentLinkedQueue;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class DocFolder {
	/** 父目录 */
	private DocFolder parent;
	/** 本地文件 */
	private File dir;
	private CreateFolderRequest createFolderRequest;
	private CreateFolderResponse createFolderResponse;
	/**子文件*/
	private ConcurrentLinkedQueue<DocFile> files;
	/**子目录*/
	private ConcurrentLinkedQueue<DocFolder> folders;

	@Override
	public String toString() {
		return "DocFolder{" +
				"parent=" + (parent == null?null:parent.getDir()) +
				", dir=" + dir +
				", createFolderRequest=" + createFolderRequest +
				", createFolderResponse=" + createFolderResponse +
				", files=" + files +
				", folders=" + folders +
				'}';
	}
}
