package entity.inte.mycloud.folder;

import com.alibaba.fastjson.JSONObject;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CreateFolderResponse {
	/** 创建文件夹的token */
	private String token;
	/** 创建文件夹的访问url */
	private String url;

	public static CreateFolderResponse parseObject(String jsonData) {
		return JSONObject.parseObject(jsonData, CreateFolderResponse.class);
	}
}
