package entity.inte.mycloud.folder;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class GetFolderChildrenResponse {
	private FolderFile[] files;
	private String next_page_token;
	private Boolean has_more;
}
