package entity.inte.mycloud.folder;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 文件夹清单列表
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class FolderFile {
	private String token;
	private String name;
	private String type;
	private String parent_token;
	private String url;
	private ShortcutInfo shortcut_info;
}
