package entity.inte.mycloud.folder;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 快捷方式文件信息
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ShortcutInfo {
	/** 快捷方式指向的原文件类型 */
	private String target_type;
	/** 快捷方式指向的原文件token */
	private String target_token;
}
