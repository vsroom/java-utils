package entity.inte.mycloud.folder;

import com.alibaba.fastjson.JSONObject;
import entity.inte.mycloud.imports.CreateImportTasksResponse;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CreateFolderRequest {
	/** 文件夹名称 */
	private String name;
	/** 父文件夹token */
	private String folder_token;

	public static CreateFolderRequest parseObject(String jsonData) {
		return JSONObject.parseObject(jsonData, CreateFolderRequest.class);
	}
}
