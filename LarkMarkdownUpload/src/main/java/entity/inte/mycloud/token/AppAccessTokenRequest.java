package entity.inte.mycloud.token;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AppAccessTokenRequest {
	private String app_id;
	private String app_secret;
}
