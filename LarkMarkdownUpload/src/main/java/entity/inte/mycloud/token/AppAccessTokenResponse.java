package entity.inte.mycloud.token;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AppAccessTokenResponse {
	private Integer code;
	private String msg;
	/** 应用访问凭证 */
	private String app_access_token;
	/** app_access_token 的过期时间，单位为秒 */
	private Integer expire;

	public String getAuthorization() {
		return app_access_token == null ? null : "Bearer " + app_access_token;
	}
}
