package entity.inte.mycloud.doc.block;

import com.alibaba.fastjson.JSONObject;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 文字
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class TextRun {
	private String content;
	private TextElementStyle text_element_style;

	public static TextRun parseObject(String jsonData) {
		TextRun textRun = JSONObject.parseObject(jsonData, TextRun.class);
		textRun.setText_element_style(TextElementStyle.parseObject(JSONObject.parseObject(jsonData).getString("text_element_style")));
		return textRun;
	}
}
