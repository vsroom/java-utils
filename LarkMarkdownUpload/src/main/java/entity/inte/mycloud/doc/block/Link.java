package entity.inte.mycloud.doc.block;

import com.alibaba.fastjson.JSONObject;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 链接
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Link {
	/** 超链接指向的 url (需要 url_encode) */
	private String url;

	public static Link parseObject(String jsonData) {
		return JSONObject.parseObject(jsonData, Link.class);
	}
}
