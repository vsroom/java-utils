package entity.inte.mycloud.doc.block;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * \@用户
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class MentionUser {
	private String user_id;
	private TextElementStyle text_element_style;
}
