package entity.inte.mycloud.doc.block;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 内联附件
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Element {
	private TextRun text_run;
	private MentionUser mention_user;
	private MentionDoc mention_doc;
	private Reminder reminder;
	private InlineFile file;
	private InlineBlock inline_block;
	private Equation equation;

}
