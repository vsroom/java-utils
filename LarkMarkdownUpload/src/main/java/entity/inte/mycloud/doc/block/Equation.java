package entity.inte.mycloud.doc.block;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 公式
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Equation {
	private String content;
	private TextElementStyle text_element_style;
}
