package entity.inte.mycloud.doc.block;

import com.alibaba.fastjson.JSONObject;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 文本局部样式
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class TextElementStyle {
	private Boolean bold;
	private Boolean italic;
	private Boolean strikethrough;
	private Boolean underline;
	private Boolean inline_code;
	private Integer background_color;
	private Integer text_color;
	private Link link;
	private String[] comment_ids;

	public static TextElementStyle parseObject(String jsonData) {
		TextElementStyle textElementStyle = JSONObject.parseObject(jsonData, TextElementStyle.class);
		textElementStyle.setLink(Link.parseObject(JSONObject.parseObject(jsonData).getString("link")));
		return textElementStyle;
	}
}
