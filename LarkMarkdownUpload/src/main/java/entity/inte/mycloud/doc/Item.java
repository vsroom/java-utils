package entity.inte.mycloud.doc;

import entity.inte.mycloud.doc.block.DocImage;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Item {
	private String block_id;
	private String parent_id;
	private String[] children;
	private Integer block_type;
	private DocImage image;

}
