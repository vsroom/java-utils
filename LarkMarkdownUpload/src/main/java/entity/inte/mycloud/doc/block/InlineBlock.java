package entity.inte.mycloud.doc.block;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 内联 block
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class InlineBlock {
	private String block_id;
	private TextElementStyle text_element_style;

}
