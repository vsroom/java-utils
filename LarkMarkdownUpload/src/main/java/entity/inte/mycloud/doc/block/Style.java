package entity.inte.mycloud.doc.block;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Style {
	private Integer align;
	private Boolean done;
	private Boolean folded;
	private Integer language;
	private Boolean wrap;
}
