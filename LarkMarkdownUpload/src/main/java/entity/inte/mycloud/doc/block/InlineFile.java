package entity.inte.mycloud.doc.block;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 内联附件
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class InlineFile {
	/**
	 * 附件 token
	 */
	private String file_token;
	/**
	 * 当前文档中该附件所处的 block 的 id
	 */
	private String source_block_id;
	private TextElementStyle text_element_style;
}
