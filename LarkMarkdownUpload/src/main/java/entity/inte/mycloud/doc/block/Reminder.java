package entity.inte.mycloud.doc.block;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 日期提醒
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Reminder {
	private String create_user_id;
	private Boolean is_notify;
	private Boolean is_whole_day;
	private String  expire_time;
	private String  notify_time;
	private TextElementStyle text_element_style;
}
