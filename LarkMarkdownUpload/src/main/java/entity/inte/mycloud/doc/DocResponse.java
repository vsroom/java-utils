package entity.inte.mycloud.doc;

import com.alibaba.fastjson.JSONObject;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DocResponse {
	/** 文档的 Block 信息，JSON数据，种类太多，这里解析一部分，剩下的用String表示 */
	private Item[] items;
	/** 分页标记，当 has_more 为 true 时，会同时返回新的 page_token，否则不返回 page_token */
	private String page_token;
	/** 是否还有更多项 */
	private Boolean has_more;

	public static DocResponse parseObject(String jsonData) {
		return JSONObject.parseObject(jsonData, DocResponse.class);
	}
}
