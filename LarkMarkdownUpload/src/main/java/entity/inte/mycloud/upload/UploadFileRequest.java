package entity.inte.mycloud.upload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.File;

/**
 * <a href="https://open.feishu.cn/document/uAjLw4CM/ukTMukTMukTM/reference/drive-v1/file/upload_all">上传文件</a>
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UploadFileRequest {
	/** 文件名 */
	private String file_name;
	/** 上传点类型 */
	private String parent_type = "explorer";
	/** 文件夹token */
	private String parent_node;
	/** 文件大小（以字节为单位） */
	private Long size;
	/** 文件adler32校验和(可选) */
	private String checksum = null;
	/** 文件二进制内容 */
	private File file;
}
