package entity.inte.mycloud.upload;

import com.alibaba.fastjson.JSONObject;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UploadFileResponse {
	private String file_token;

	public static UploadFileResponse parseObject(String jsonData) {
		return JSONObject.parseObject(jsonData, UploadFileResponse.class);
	}
}