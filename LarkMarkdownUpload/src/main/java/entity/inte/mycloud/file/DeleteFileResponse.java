package entity.inte.mycloud.file;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import entity.inte.ResponseData;
import entity.inte.mycloud.doc.DocResponse;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * <a href="https://open.feishu.cn/document/uAjLw4CM/ukTMukTMukTM/reference/drive-v1/file/delete">删除文件</a>
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class DeleteFileResponse {
	/** 异步任务id，删除文件夹时返回 */
	private String task_id;

	public static DeleteFileResponse parseObject(String jsonData) {
		return JSONObject.parseObject(jsonData, DeleteFileResponse.class);
	}
}
