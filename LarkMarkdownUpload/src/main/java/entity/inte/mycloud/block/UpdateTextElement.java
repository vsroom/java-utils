package entity.inte.mycloud.block;

import entity.inte.mycloud.doc.block.Element;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UpdateTextElement {
	private String block_id;
	private TextElement update_text_elements;

	@Data
	@NoArgsConstructor
	@AllArgsConstructor
	public static class TextElement {
		private Element[] elements;
	}
}
