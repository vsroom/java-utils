package entity.inte.mycloud.block;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ReplaceImage {
	private String block_id;
	private Image replace_image;

	@Data
	@NoArgsConstructor
	@AllArgsConstructor
	public static class Image {
		private String token;
	}
}
