package entity.inte.mycloud.imports;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import entity.inte.ResponseData;
import entity.inte.mycloud.file.DeleteFileResponse;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 创建导入任务响应体
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CreateImportTasksResponse {
	private String ticket;

	public static CreateImportTasksResponse parseObject(String jsonData) {
		return JSONObject.parseObject(jsonData, CreateImportTasksResponse.class);
	}
}
