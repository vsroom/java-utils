package entity.inte.mycloud.imports;

import com.alibaba.fastjson.JSONObject;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class QueryImportTasksResponse {
	private Result result;

	@Data
	@NoArgsConstructor
	@AllArgsConstructor
	public static class Result {
		private String ticket;
		private String type;
		private Integer job_status;
		private String job_error_msg;
		private String token;
		private String url;
		private String[] extra;
	}


	public static QueryImportTasksResponse parseObject(String jsonData) {
		JSONObject jsonObject = JSONObject.parseObject(jsonData);
		Result r = jsonObject.getObject("result", Result.class);
		return new QueryImportTasksResponse(r);
	}
}
