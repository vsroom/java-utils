package entity.inte.mycloud.imports;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * <a href="https://open.feishu.cn/document/uAjLw4CM/ukTMukTMukTM/reference/drive-v1/import_task/create">创建导入任务</a>
 */
@EqualsAndHashCode(callSuper = false)
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CreateImportTasksRequest {
	/** 导入文件格式后缀 */
	private String file_extension;
	/** 导入文件Drive FileToken */
	private String file_token;
	/** 导入目标云文档格式 */
	private String type = "docx";
	/** 导入目标云文档文件名 ，若为空使用Drive文件名 */
	private String file_name = null;
	/** 挂载点 */
	private Point point;

	@Data
	@NoArgsConstructor
	@AllArgsConstructor
	public static class Point {
		private Integer mount_type = 1;
		private String mount_key;
	}
}
