package entity.inte.mycloud.imports;


import lombok.Getter;

import java.util.HashMap;

/**
 * 任务状态描述
 */
@Getter
public class JobStatus {
	private static final HashMap<Integer, String> DESCRIPTIONS;

	static {
		DESCRIPTIONS = new HashMap<>();
		DESCRIPTIONS.put(0, "成功");
		DESCRIPTIONS.put(1, "初始化");
		DESCRIPTIONS.put(2, "处理中");
		DESCRIPTIONS.put(3, "内部错误");
		DESCRIPTIONS.put(100, "导入文档已加密");
		DESCRIPTIONS.put(101, "内部错误");
		DESCRIPTIONS.put(102, "内部错误");
		DESCRIPTIONS.put(103, "内部错误");
		DESCRIPTIONS.put(104, "租户容量不足");
		DESCRIPTIONS.put(105, "文件夹节点太多");
		DESCRIPTIONS.put(106, "内部错误");
		DESCRIPTIONS.put(108, "处理超时");
		DESCRIPTIONS.put(109, "内部错误");
		DESCRIPTIONS.put(110, "无权限");
		DESCRIPTIONS.put(112, "格式不支持");
		DESCRIPTIONS.put(113, "office格式不支持");
		DESCRIPTIONS.put(114, "内部错误");
		DESCRIPTIONS.put(115, "导入文件过大");
		DESCRIPTIONS.put(116, "目录无权限");
		DESCRIPTIONS.put(117, "目录已删除");
		DESCRIPTIONS.put(118, "导入文件和任务指定后缀不匹配");
		DESCRIPTIONS.put(119, "目录不存在");
		DESCRIPTIONS.put(120, "导入文件和任务指定文件类型不匹配");
		DESCRIPTIONS.put(121, "导入文件已过期");
		DESCRIPTIONS.put(122, "创建副本中禁止导出");
		DESCRIPTIONS.put(5000, "内部错误");
		DESCRIPTIONS.put(7000, "docx block 数量超过系统上限");
		DESCRIPTIONS.put(7001, "docx block 层级超过系统上线");
		DESCRIPTIONS.put(7002, "docx block 大小超过系统上限");
	}

	public static String getDescription(Integer jobStatus) {
		return DESCRIPTIONS.get(jobStatus);
	}
}
