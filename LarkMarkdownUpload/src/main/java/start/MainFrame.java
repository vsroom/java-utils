package start;

import com.formdev.flatlaf.FlatLightLaf;
import net.miginfocom.swing.MigLayout;
import session.SessionData;

import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import java.awt.*;
import java.io.File;

public class MainFrame extends JFrame {
	private static MainFrame INSTANCE;

	private final JLabel appIdLabel = new JLabel("app id");
	private final JTextField appIdField = new JTextField(30);
	private final JLabel appSecretLabel = new JLabel("app secret");
	private final JTextField appSecretField = new JTextField(30);

	private final JLabel rootFolderLabel = new JLabel("folder token");
	private final JTextField rootFolderField = new JTextField(30);
	private final JLabel dirLabel = new JLabel("上传目录/文件");
	private final JTextField dirField = new JTextField(30);
	private final JButton dirButton = new JButton("选择");
	private final JFileChooser fileChooser = new JFileChooser();
	private final JButton uploadButton = new JButton("上传");
	private final JTextPane logPane = new JTextPane();
	private final JProgressBar progressBar = new JProgressBar();


	private UploadWorker uploadWorker;

	private MainFrame() {
		setTitle("飞书上传markdown");
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		Container contentPane = getContentPane();
		contentPane.setLayout(new MigLayout("wrap 2", "grow"));

		initComponent();
		addListener();

		contentPane.add(appIdLabel, "right");
		contentPane.add(appIdField, "growx");
		contentPane.add(appSecretLabel, "right");
		contentPane.add(appSecretField, "growx");
		contentPane.add(rootFolderLabel, "right");
		contentPane.add(rootFolderField, "growx");
		contentPane.add(dirLabel, "right");
		contentPane.add(dirField, "split 2");
		contentPane.add(dirButton);
		contentPane.add(uploadButton, "span 2,center");
		contentPane.add(new JScrollPane(logPane), "span 2, growx, h 400::");
		contentPane.add(progressBar, "span 2, growx");

		pack();
		setLocationRelativeTo(null);
	}

	public static MainFrame getInstance() {
		if (INSTANCE == null)
			INSTANCE = new MainFrame();
		return INSTANCE;
	}

	private void initComponent() {
		appIdField.setText(SessionData.APP_ACCESS_TOKEN_REQUEST.getApp_id());
		appSecretField.setText(SessionData.APP_ACCESS_TOKEN_REQUEST.getApp_secret());
		rootFolderField.setText(SessionData.ROOT_FOLDER_TOKEN);

		logPane.setContentType("text/html");
		logPane.setEditable(false);
		logPane.setText("<html>");
		JPopupMenu popupMenu = new JPopupMenu();
		JMenuItem menuItem = new JMenuItem("清空日志");
		menuItem.addActionListener(e -> logPane.setText(""));
		popupMenu.add(menuItem);
		logPane.setComponentPopupMenu(popupMenu);

		fileChooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
		fileChooser.setFileFilter(new FileFilter() {
			@Override
			public boolean accept(File f) {
				return f.isDirectory() || f.getName().endsWith(".md");
			}

			@Override
			public String getDescription() {
				return "选择目录或者md文件";
			}
		});
	}

	private void addListener() {
		dirButton.addActionListener(e -> {
			if (fileChooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
				dirField.setText(fileChooser.getSelectedFile().getAbsolutePath());
			}
		});

		uploadButton.addActionListener(e -> {
			String appId = appIdField.getText();
			String appSecret = appSecretField.getText();
			String rootFolder = rootFolderField.getText();
			String dir = dirField.getText();

			if ("".equals(appId) || "".equals(appSecret)) {
				JOptionPane.showConfirmDialog(this, "应用唯一标识和应用秘钥不能为空", "提示",
						JOptionPane.DEFAULT_OPTION, JOptionPane.ERROR_MESSAGE);
				return;
			}
			SessionData.APP_ACCESS_TOKEN_REQUEST.setApp_id(appId);
			SessionData.APP_ACCESS_TOKEN_REQUEST.setApp_secret(appSecret);

			if ("".equals(rootFolder) || "".equals(dir)) {
				JOptionPane.showConfirmDialog(this, "文件夹token和目录都不能为空", "提示",
						JOptionPane.DEFAULT_OPTION, JOptionPane.ERROR_MESSAGE);
				return;
			}
			File dirFile = new File(dir);
			if (!dirFile.exists()) {
				JOptionPane.showConfirmDialog(MainFrame.getInstance(), dir + " 不是文件也不是目录", "提示",
						JOptionPane.DEFAULT_OPTION, JOptionPane.ERROR_MESSAGE);
				return;
			}

			// 初始化组件
			progressBar.setValue(0);

			// 任务开始
			uploadWorker = new UploadWorker(rootFolder, dirFile);
			uploadWorker.execute();
			uploadButton.setEnabled(false);
		});
	}

	public JProgressBar getProgressBar() {
		return progressBar;
	}

	public JButton getUploadButton() {
		return uploadButton;
	}

	public JTextPane getLogPane() {
		return logPane;
	}

	public void setUploadWorker(UploadWorker uploadWorker) {
		this.uploadWorker = uploadWorker;
	}

	public static void main(String[] args) {
		EventQueue.invokeLater(() -> {
			FlatLightLaf.setup();
			MainFrame.getInstance().setVisible(true);
		});
	}
}
