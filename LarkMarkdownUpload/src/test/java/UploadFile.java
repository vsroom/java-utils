import entity.inte.mycloud.upload.UploadFileResponse;
import org.jsoup.Jsoup;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public class UploadFile {
	public static final String URL = "https://open.feishu.cn/open-apis/drive/v1/files/upload_all";
	public static String token = "t-g104ba06HR6WLXZWOW5ABVMWMQAHU6MBFLM6VCVH";

	public static void main(String[] args) throws IOException {
		File file = new File("D:\\MyDefault\\desktop\\basics.md");

		String jsonData = Jsoup.connect(URL)
				.header("Authorization", "Bearer " + token) //cookie
				.header("Content-Type", "multipart/form-data;")
				.data("file_name", file.getName()) //文件名
				.data("parent_type", "explorer") //上传点类型=云空间
				.data("parent_node", "fldcnYWPuFYcUYNB43uwXL2ey65") //文件夹token
				.data("size", file.length() + "") //文件大小
				.data("file", file.getName(), new FileInputStream(file)) //二进制文件数据
				.ignoreContentType(true)
				.post()
				.text();
	}
}
