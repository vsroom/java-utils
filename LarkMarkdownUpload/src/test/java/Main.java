import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

public class Main {
	private static final ExecutorService executorService = Executors.newCachedThreadPool();

	public static void main(String[] args) throws IOException, InterruptedException {
		ArrayList<Callable<Void>> tasks = new ArrayList<>();

		IntStream.rangeClosed(1, 7)
				.forEach(value -> tasks.add(() -> {
					TimeUnit.SECONDS.sleep(value);
					System.out.println(value + " 号结束");
					throw new RuntimeException("cuowi");
					// return null;
				}));
		executorService.invokeAll(tasks);
		executorService.shutdownNow();

		System.out.println("完成");
	}
}