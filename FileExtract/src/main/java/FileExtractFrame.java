import com.formdev.flatlaf.FlatLightLaf;
import net.miginfocom.swing.MigLayout;
import org.jdesktop.swingx.JXTextField;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.*;
import java.io.File;

public class FileExtractFrame extends JFrame {
	JXTextField ruleField = new JXTextField("文件全路径的正则匹配");
	Timer searchTimer;
	SearchWorker searchWorker;
	CopyWorker copyWorker;
	JXTextField fromField = new JXTextField("要提取的目录");
	JButton fromButton = new JButton("选择");
	JXTextField toField = new JXTextField("生成目录");
	JButton toButton = new JButton("选择");
	/** 是否保留目录结构 */
	// JCheckBox isRetainDic = new JCheckBox("是否保留目录结构");
	JFileChooser chooser = new JFileChooser();
	DefaultListModel<String> model = new DefaultListModel<>();
	JList<String> list = new JList<>(model);
	JButton extractButton = new JButton("提取");
	JLabel progress = new JLabel("0%");

	public FileExtractFrame() {
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setTitle("文件提取器");

		JPanel p = new JPanel(new MigLayout("wrap 1", "grow, center"));
		p.setPreferredSize(new Dimension(600, 600));

		p.add(ruleField, "growx");
		p.add(fromField, "split 2, growx");
		p.add(fromButton);
		p.add(toField, "split 2, growx");
		p.add(toButton);
		// p.add(isRetainDic, "wrap, left");
		p.add(extractButton, "wrap");
		p.add(new JScrollPane(list), "growx,growy, h :600:");
		p.add(progress,"left");

		setSearchTask();
		addListener();

		getContentPane().add(p);
		pack();
		setLocationRelativeTo(null);
	}

	private void addListener() {
		chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

		fromButton.addActionListener(e -> {
			if (chooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
				File f = chooser.getSelectedFile();
				fromField.setText(f.getAbsolutePath());
				searchTimer.restart();
			}
		});
		toButton.addActionListener(e -> {
			if (chooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
				File f = chooser.getSelectedFile();
				toField.setText(f.getAbsolutePath());
			}
		});

		// 提取
		extractButton.addActionListener(e -> {
			if (searchWorker != null && !searchWorker.isDone()) {
				JOptionPane.showConfirmDialog(this, "请等待搜索完毕", "提示", JOptionPane.DEFAULT_OPTION);
				return;
			}
			copyWorker = new CopyWorker(progress,model, extractButton, new File(toField.getText()));
			copyWorker.execute();
			extractButton.setEnabled(false);
		});

		ruleField.getDocument().addDocumentListener(new DocumentListener() {
			@Override
			public void insertUpdate(DocumentEvent e) {
				searchTimer.restart();
			}

			@Override
			public void removeUpdate(DocumentEvent e) {
				searchTimer.restart();
			}

			@Override
			public void changedUpdate(DocumentEvent e) {
			}
		});
	}

	private void setSearchTask() {
		searchTimer = new Timer(700, e -> {
			File f = new File(fromField.getText());
			if (!f.isDirectory()) return;
			if (searchWorker != null && !searchWorker.isDone())
				searchWorker.cancel(true);
			searchWorker = new SearchWorker(f, ruleField.getText(), model);
			searchWorker.execute();
		});
		searchTimer.setRepeats(false);
	}

	private void endSearchTask() {
		if (searchTimer != null) {
			searchTimer.stop();
			searchTimer = null;
		}
	}

	public static void main(String[] args) {
		EventQueue.invokeLater(() -> {
			FlatLightLaf.setup();
			new FileExtractFrame().setVisible(true);
		});
	}
}
