import javax.swing.*;
import java.io.*;

class CopyWorker extends SwingWorker<Integer, Integer> {
	DefaultListModel<String> model;
	File dir;
	JButton b;
	JLabel progress;

	public CopyWorker(JLabel progress, DefaultListModel<String> model, JButton b, File dir) {
		this.progress = progress;
		this.model = model;
		this.dir = dir;
		this.b = b;
	}

	@Override
	protected Integer doInBackground() {
		try {
			if (model.getSize() == 0) return null;

			byte[] b = new byte[1024];
			int length;
			String dirStr = this.dir.getAbsolutePath();
			for (int i = 0, len = model.getSize(); i < len; i++) {
				String oldFilename = model.get(i);
				File f = new File(oldFilename);

				File newFile = new File(findSame(oldFilename, dirStr));

				File parentFile = newFile.getParentFile();
				if (!parentFile.isDirectory()) parentFile.mkdirs();

				try (BufferedInputStream bis = new BufferedInputStream(new FileInputStream(f.getAbsolutePath()));
				     BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(newFile))) {
					while ((length = bis.read(b)) != -1) {
						bos.write(b, 0, length);
					}
				}
				publish(i);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	protected void process(java.util.List<Integer> chunks) {
		progress.setText(String.format("%.2f%%", chunks.get(chunks.size() - 1) * 1f / model.size() * 100));
	}

	@Override
	protected void done() {
		b.setEnabled(true);
		JOptionPane.showConfirmDialog(null, "拷贝完成", "提示", JOptionPane.DEFAULT_OPTION);
	}

	public static String findSame(String from, String to) {
		char[] c1 = from.toCharArray();
		char[] c2 = to.toCharArray();
		int index = 0;
		for (int i = 0; i < c1.length; i++) {
			if (c1[i] != c2[i]) {
				break;
			} else if (c1[i] == '\\' || c1[i] == '/') {
				index = i;
			}
		}
		return to + from.substring(index);
	}
}
