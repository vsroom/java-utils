import java.io.File;
import java.util.Objects;
import java.util.stream.Stream;

public class FileExtractUtil {
	/**
	 * 获取该文件及所有子文件流
	 */
	public static Stream<File> descendants(File f) {
		return Stream.of(Objects.requireNonNullElse(f.listFiles(), new File[0]))
				.flatMap(m -> Stream.concat(Stream.of(m), descendants(m)))
				.filter(File::isFile);
	}
}
